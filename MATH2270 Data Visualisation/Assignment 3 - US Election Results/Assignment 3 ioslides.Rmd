---
title: "US Presidential Election Results 2008-2016"
subtitle: "Data Visualisation Assignment 3"
author: "Scott Keene (s3686673)"
# output: slidy_presentation
output: ioslides_presentation
# output: html_document
urlcolor: blue
---

```{r setup, include=FALSE}

knitr::opts_chunk$set(warning = FALSE, echo = FALSE, message = FALSE)
knitr::opts_knit$set(root.dir = "C:\\Users\\scott\\OneDrive\\RMIT\\2020s2\\MATH2270 Data Visualisation\\Assignment 3\\data")


```

```{r}
# Import libraries
library(readr)
library(usmap)
library(ggplot2)
library(dplyr)
library(magrittr)
library(gridExtra)
library(cowplot)
library(scales)

```


```{r}

# Import Data from https://dataverse.harvard.edu/dataverse/medsl_president

data <- read_csv("state_level\\1976-2016-president.csv") %>% 
  filter(writein == FALSE) %>% 
  mutate(party = case_when(
                  substr(party, 1, 3) == "rep" ~ "Republican",
                  substr(party, 1, 3) == "dem" ~ "Democrat",
                  TRUE ~ party
                          ),
         party_mult = case_when(
                        party == "Republican" ~ -1,
                        party == "Democrat"~ 1,
                        TRUE ~ 0
                              ),
         percent = as.integer(round(candidatevotes / totalvotes,3) * 100),
         pctParty = percent * party_mult
         ) %>% 
  select(year,
         state,
         state_po,
         # state_fips,
         # state_cen,
         # state_ic,
         # office,
         candidate,   
         party,
         # writein,
         candidatevotes,
         totalvotes,
         pctParty,
         percent
         # version,
         # notes  
  )

# Prepare data by years

# 2008 Election Results

winner2008 <- data %>%  
  filter(year == 2008) %>% 
  group_by(state, state_po) %>% 
  top_n(n=1) %>% 
  select(state,
         state_po,
         party2008 = party,
         candidate2008 = candidate,
         percent2008 = percent,
         pctParty2008 = pctParty)


# 2012 Election Results

winner2012 <- data %>%  
  filter(year == 2012) %>% 
  group_by(state, state_po) %>% 
  top_n(n=1) %>% 
  select(state,
         state_po,
         party2012 = party,
         candidate2012 = candidate,
         percent2012 = percent,
         pctParty2012 = pctParty)


# 2016 Election Results

winner2016 <- data %>%  
  filter(year == 2016) %>% 
  group_by(state, state_po) %>% 
  top_n(n=1) %>% 
  select(state,
         state_po,
         party2016 = party,
         candidate2016 = candidate,
         percent2016 = percent,
         pctParty2016 = pctParty)

results <- winner2008 %>% 
  inner_join(winner2012,
             by = "state_po") %>% 
  inner_join(winner2016,
             by = "state_po") %>% 
  mutate(swing2012 = (party2008 != party2012),
         swing2016 = (party2012 != party2016),
         swingPct2012 = percent2012 - percent2008,
         swingPct2016 = percent2016 - percent2012,
         swingTo2012 = case_when(swing2012 == TRUE ~ party2012, TRUE ~ "none"),
         swingTo2016 = case_when(swing2016 == TRUE ~ party2016, TRUE ~ "none")) %>% 
  select(state_po,
         state,
         party2008,
         candidate2008,
         percent2008,
         pctParty2008,
         party2012,
         candidate2012,
         percent2012,
         pctParty2012,
         swing2012,
         swingTo2012,
         swingPct2012,
         party2016,
         candidate2016,
         percent2016,
         pctParty2016,
         swing2016,
         swingTo2016,
         swingPct2016
         ) 

# 2012 swing States captured

swingstates2012 = results %>% 
  # filter(swing2012 == TRUE) %>% 
  select(state_po, 
         state,
         party2008,
         candidate2008,
         percent2008,
         pctParty2008,
         party2012,
         candidate2012,
         percent2012,
         pctParty2012,
         swingPct2012) %>% 
  mutate(change = percent2012 - percent2008)



# 2016 swing States captured

swingstates2016 = results %>% 
  # filter(swing2016 == TRUE) %>% 
  select(state_po, 
         state,
         party2012,
         candidate2012,
         percent2012,
         pctParty2012,
         party2016,
         candidate2016,
         percent2016,
         pctParty2016,
         swingPct2016) %>% 
  mutate(change = percent2016 - percent2012)



#TOP_N and SLICE_MAX Not working!!!!

top_2012 <- results %>% 
  select(state_po, state, party2012,swingPct2012) %>% 
  arrange(desc(swingPct2012)) 
top_2012 <- top_2012[1:3,]
  
bot_2012 <- results %>% 
  select(state_po, state, party2012,swingPct2012) %>% 
  arrange(swingPct2012)
bot_2012 <- bot_2012[1:3,]

winsLoses2012 <- rbind(top_2012[1:3,], bot_2012[1:3,] ) %>% 
  arrange(desc(abs(swingPct2012)))



top_2016 <- results %>% 
  select(state_po, state, party2016,swingPct2016) %>% 
  arrange(desc(swingPct2016)) 
top_2016 <- top_2016[1:3,]
  
bot_2016 <- results %>% 
  select(state_po, state, party2016,swingPct2016) %>% 
  arrange(swingPct2016)
bot_2016 <- bot_2016[1:3,]

winsLoses2016 <- rbind(top_2016[1:3,], bot_2016[1:3,] ) %>% 
  arrange(desc(abs(swingPct2016)))
```


## 2008 US Presidential Election Results

- In 2008 Barack Obama won the US Presidential election for the Democrats.

```{r}
  map2008 <-plot_usmap(
    data = results,
    values = "party2008",
    regions = "states",
    labels = TRUE,
    label_color = "white") + 
  
    scale_fill_manual(values = c("Republican" = "red",  "Democrat" = "blue"),
                        name = "State Winner")  +
                        
    # labs(title = "2008 US Presidential Election Results.") +
  
    theme(legend.position = "right")  

  map2008$layers[[2]]$aes_params$size <- 3
  map2008
  
```


## 2012 Election results


- In 2012 Barack Obama again won again with more states.

- This time he gained the states of Indiana and North Carolina.


```{r}
 
map2012 <-plot_usmap(
  data = results,
  values = "party2012",
  regions = "states",
  labels = TRUE,
  label_color = "white") + 

  scale_fill_manual(values = c("Republican" = "red",  "Democrat" = "blue"),
                      name = "State Winner")  +
                      

  theme(legend.position = "right")  

map2012$layers[[2]]$aes_params$size <- 3
map2012



```


## 2012 Swing States

- In 2012, Indiana and North Carolina flipped from Democrat to Republican

```{r}
swing2012 <-plot_usmap(
  data = results,
  # include = c(swingstates2012$state_po),
  values = "swingTo2012",
  regions = "states",
  labels = TRUE,
  label_color = "white") + 

  scale_fill_manual(values = c("Republican" = "red",  "Democrat" = "blue", "none" = "grey69"),
                      name = "State Swing")  +
                      

  theme(legend.position = "right")  
# , panel.background = element_rect(fill = "blue")
swing2012$layers[[2]]$aes_params$size <- 3
swing2012


```

## 2012 Largest Gains

- The top 3 states with the greatest gain in votes in 2012 were Utah (Republican), West Virginia (Republican), and Montana (Republican).


```{r}
# Barchart of the bigggest Losses
winners <- ggplot(top_2012, 
                 aes(x=reorder(state, -swingPct2012), y = swingPct2012,
                     fill = party2012)
                 ) +
  labs(y = "% Loss",
       x = "State",
       title = "Top 3 Gains"
       ) +
  geom_bar(stat="identity", width = 0.5) +
  geom_text(aes(label=paste0(swingPct2012, "%")),
            size = 6,
            hjust = 1.2,
            color = "white")+
  scale_fill_manual("Party", values = c("Republican" = "red", "Democrat" = "blue")) +
  coord_flip() 

winners
```


## 2012 Largest Losses
- The top 3 states with the greatest loss of votes in 2012 were Alaska (Republican), Illinois (Democrat), and New Mexico (Democrat).

```{r}
# Barchart of the bigggest Losses
losers <- ggplot(bot_2012, 
                 aes(x=reorder(state, -swingPct2012), y = abs(swingPct2012),
                     fill = party2012)
                 ) +
  labs(y = "% Loss",
       x = "State",
       title = "Top 3 Losses"
       ) +
  geom_bar(stat="identity", width = 0.5) +
  geom_text(aes(label=paste0(abs(swingPct2012), "%")), 
            size = 6,
            hjust = 1.2,
            color = "white")+
  scale_fill_manual("Party", values = c("Republican" = "red", "Democrat" = "blue")) +
  coord_flip() 
losers

  
```




## 2016 Election results


- In 2016 the Democrat representative Hillary Clinton lost to Republican Donald Trump.


```{r}
 
map2016 <-plot_usmap(
  data = results,
  values = "party2016",
  regions = "states",
  labels = TRUE,
  label_color = "white") + 

  scale_fill_manual(values = c("Republican" = "red",  "Democrat" = "blue"),
                      name = "State Winner")  +
                      

  theme(legend.position = "right")  

map2016$layers[[2]]$aes_params$size <- 3
map2016



```


## 2016 Swing States

- In 2016 Indiana, Wisconsin, Missouri, Ohio, Pensylvania, and Florida flipped from Democrat to Republican

```{r}
swing2016 <-plot_usmap(
  data = results,
  # include = c(swingstates2016$state_po),
  values = "swingTo2016",
  regions = "states",
  labels = TRUE,
  label_color = "white") + 

  scale_fill_manual(values = c("Republican" = "red",  "Democrat" = "blue", "none" = "grey69"),
                      name = "State Swing")  +
                      

  theme(legend.position = "right")  

swing2016$layers[[2]]$aes_params$size <- 3
swing2016


```

## 2016 Largest Gains

- The top 3 states with the greatest gain in votes in 2016 were South Dakota (Republican), South Dakota (Republican), and West Virginia (Republican).


```{r}
# Barchart of the bigggest Losses
winners <- ggplot(top_2016, 
                 aes(x=reorder(state, -swingPct2016), y = swingPct2016,
                     fill = party2016)
                 ) +
  labs(y = "% Loss",
       x = "State",
       title = "Top 3 Gains"
       ) +
  geom_bar(stat="identity", width = 0.5) +
  geom_text(aes(label=paste0(swingPct2016, "%")),
            size = 6,
            hjust = 1.2,
            color = "white")+
  scale_fill_manual("Party", values = c("Republican" = "red", "Democrat" = "blue")) +
  coord_flip() 

winners
```


## 2016 Largest Losses

- The top 3 states with the greatest loss of votes in 2016 were Hawaii (Democrat), Vermont (Democrat), and Utah (Republican).

```{r}
# Barchart of the bigggest Losses
losers <- ggplot(bot_2016, 
                 aes(x=reorder(state, -swingPct2016), y = abs(swingPct2016),
                     fill = party2016)
                 ) +
  labs(y = "% Loss",
       x = "State",
       title = "Top 3 Losses"
       ) +
  geom_bar(stat="identity", width = 0.5) +
  geom_text(aes(label=paste0(abs(swingPct2016), "%")), 
            size = 6,
            hjust = 1.2,
            color = "white")+
  scale_fill_manual("Party", values = c("Republican" = "red", "Democrat" = "blue")) +
  coord_flip() 
losers

  
```

## Reference

- MIT Election Data and Science Lab. (2017), *U.S. President 1976–2016*. [Data File]. Retrieved from https://doi.org/10.7910/DVN/42MVDX
