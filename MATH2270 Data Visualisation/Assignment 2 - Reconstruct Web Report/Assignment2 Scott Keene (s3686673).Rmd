---
title: "Assignment 2"
subtitle: "Deconstruct, Reconstruct Web Report"
author: "Scott Keene (s3686673)"
output: html_document
urlcolor: blue
---

```{r setup, include=FALSE}
# Do not change these settings!
knitr::opts_chunk$set(warning = FALSE, message = FALSE)
```

### {.tabset} 

Click the **Original**, **Code** and **Reconstruction** tabs to read about the issues and how they were fixed. 


#### Original

<br>
<center>
<img src="TLPF Asset Allocation Actual vs Target.png" width="80%">
</center>
<center>*Timor-Leste Ministry of Finance. (2019).*</center>
<br>

**Objective**

The original data visualisation targeted investors, government officials, and members of the public with interest in the financial allocations in the Timor-Leste Petroleum Fund. The visualisation's objective is to allow comparison of the targeted benchmark asset allocation of the fund, and compare this with the actual allocations as at year end. 

**The visualisation chosen had the following three main issues:**

* The target and actual allocations are shown on two separate doughnut charts making it difficult to compare the two sets of values.

* The visualisation breaks down the Actual Allocations by Fund Manger. This level of breakdown is not present for the benchmark allocations, making comparison between benchmark and actual allocations difficult. 

* As doughnut charts are used, it is difficult to compare allocations using angle and area without reading the numeric values.

**Reference**

* Timor-Leste Ministry of Finance. (2019). *Timor-Leste Petroleum Fund Annual Report 2018 *. Retrieved fromhttps://www.mof.gov.tl/wp-content/uploads/2019/08/2018-annual-report-ENGLISH.pdf

#### Code

The following code was used to fix the issues identified in the original. 

```{r}
library(ggplot2)
library(dplyr)

# Read source data extracted from report page 11 to CSV.
# Remove Manager, as it is not included in the Benchmark allocation values to compare with.
# Convert allocation to percentage
data <- readr::read_csv("Timor Leste Petroleum1.csv") %>% 
  group_by(AssetClass, BenchmarkActual) %>% 
  summarise(Allocation = sum(Allocation) / 100)


# Factor BenchmarkActual so order Benchmark first
data$BenchmarkActual <- data$BenchmarkActual %>% 
  factor(levels = c("Benchmark", "Actual"), ordered = TRUE)

# Determine AssetClass Factor level based on largest benchmark Allocation
AssetClassLevels <- data%>% 
  filter(BenchmarkActual == "Benchmark") %>% 
  group_by(AssetClass) %>% 
  summarise(TotalAlloc = sum(Allocation)) %>% 
  arrange(TotalAlloc) %>% 
  pull(AssetClass)

# Factor AssetClass by largest benchmark allocation
data$AssetClass <- data$AssetClass %>% 
  factor(levels = as.list(AssetClassLevels), ordered = TRUE)

p1 <- ggplot(data = data, aes(x = AssetClass, y = Allocation, fill = BenchmarkActual)) + 
  scale_y_continuous(labels = scales::percent_format(accuracy = 1L))+
  geom_bar(stat = "identity",position = position_dodge2(reverse = TRUE)) +
  labs(title = "Asset Allocation vs Benchmarks",
       x = "Asset Class",
       y = "Allocation Percentage") +
  scale_fill_manual('Allocation to:', values = c("steelblue2","turquoise2"))+
  coord_flip() 


```

**Data Reference**

* Timor-Leste Ministry of Finance. (2019). *Timor-Leste Petroleum Fund Annual Report 2018 *. Retrieved fromhttps://www.mof.gov.tl/wp-content/uploads/2019/08/2018-annual-report-ENGLISH.pdf

#### Reconstruction

The following plot fixes the main issues in the original.

```{r fig.align="center", echo = FALSE}
p1
```

